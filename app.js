angular.module("app",['ui.router','show','add','search'])
.controller("IndexCtrl",['$scope',function($scope,$state){

	$scope.contactList=[{name:'Harry',mobile:'989965654'},
	{name:'Warlock',mobile:'789564125'},
	{name:'Drow',mobile:'8459621645'}];

	console.log($scope.contactList);

	// $scope.show = function(){
	// 	$state.go('show');
	// };
	// $scope.add = function(){
	// 	$state.go('add');
	// };
	// $scope.editclick = function(){
	// 	$state.go('edit');
	// };
}
]);


angular.module('show',[]).config(function($stateProvider){
	$stateProvider.state('show',{
		url:'/show',
		templateUrl:'show/show.tpl',
		controller:'ShowCtrl'
	});
}).controller('ShowCtrl',function($scope,$state){
	console.log($scope.contactList);
	$scope.goToEditState = function(contactIndex){
		console.log("inside edit ");
		$state.go('edit',{index:contactIndex});
	};

	$scope.delete = function(deleteElement){
		$scope.contactList.splice(deleteElement,1);
	};
});

angular.module('add',[]).config(function($stateProvider){
	$stateProvider.state('add',{
		url:'/add',
		templateUrl:'add/add.tpl',
		controller:'AddCtrl'
	});
	$stateProvider.state('edit',{
		url:'/edit/:index',
		templateUrl:'add/add.tpl',
		controller:'EditCtrl'
	});

}).controller('AddCtrl',function($scope,$state){
	console.log($scope.contactList);
	$scope.add=function(newDetails){
		$scope.contactList.push(newDetails);
		$scope.contactAdded=true;
		$scope.newContact={};
		$state.go('show');	
	};
	$scope.search=function(){
		console.log("search button");
		$state.go('search');
	};

}).controller('EditCtrl',function($scope,$state,$stateParams){
	console.log("in edit section");
	var index= $stateParams.index;
	$scope.newContact=$scope.contactList[index];
	$scope.add=function(newDetails){
		$scope.contactList.splice(index,1);
		$scope.contactList.push(newDetails);
		$scope.contactAdded=true;
		$scope.newContact={};
		$state.go('show');	
	};
});

angular.module('search',[]).config(function($stateProvider){
	$stateProvider.state('search',{
		url:'/search',
		templateUrl:'search/search.tpl',
		controller:'SearchCtrl'
	});
	$stateProvider.state('search.searchResult',{
		url:'/searchResult/:haveSearchData',
		templateUrl:'search/searchresult.tpl',
		controller:'ShowSearchCtrl'
	});
}).controller('SearchCtrl',function($scope,$state){
	console.log("SearchCtrl",$scope.contactList);
	$scope.haveSearchData = [];
	$scope.search=function(searchName){
		console.log("before loop");
		console.log($scope.contactList.length);
		for(i=0;i<$scope.contactList.length;i++){
			console.log("in loop");
			if($scope.contactList[i].name === searchName){
				$scope.haveSearchData.push($scope.contactList[i]);
				console.log($scope.haveSearchData);
			}
				// else if(contactList[i].mobile === search){
				// 	$scope.haveSearchData = $scope.haveSearchData+1;
				// }
				console.log($scope.haveSearchData);
			}
		console.log("after	 loop");
		console.log("Search present in data ", $scope.haveSearchData);
		// $state.go('searchResult',{haveSearchData:$scope.haveSearchData});
	}
});
// .controller('ShowSearchCtrl',function($scope,$state){
// 	console.log($scope.stateParams);
// })